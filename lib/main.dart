import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_pay/flutter_pay.dart';
import 'dart:async';
import 'custom_icons.dart' as CustomIcon;

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  FlutterPay flutterPay = FlutterPay();
  String state = "false";
  Color color = Colors.orange;
  String res;

  @override
  void initState() {
    super.initState();
  }

  void makePayment() async {
    setState(() {
      if (color == Colors.orange) {
        color = Colors.pink;
      } else {
        color = Colors.orange;
      }
    });
    PaymentItem item = PaymentItem(name: "T-Shirt", price: 10.98);
    if (flutterPay != null) {
      String token = await flutterPay.makePayment(
        merchantIdentifier: "merchant.prototype.test",
        currencyCode: "EUR",
        countryCode: "FR",
        allowedPaymentNetworks: [
          PaymentNetwork.visa,
          PaymentNetwork.masterCard,
        ],
        paymentItems: [item],
        merchantName: "Moneytrack",
        gatewayName: "MongoPay",
      );
      print(token.length);
    }
  }

  @override
  Widget build(BuildContext context) {
    mainAxisAlignment:
    MainAxisAlignment.spaceBetween;
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Apple pay'),
        ),
        body: Container(
          child: Column(children: [
            FlatButton(
              child: Text("Can make payments with Visa and MasterCard?"),
              onPressed: () async {
                bool result = await flutterPay.canMakePaymentsWithActiveCard(
                  allowedPaymentNetworks: [
                    PaymentNetwork.visa,
                    PaymentNetwork.masterCard,
                  ],
                );
                if (result) {
                  setState(() {
                    state = "true";
                  });
                }
                print("Can make payments: $result");
              },
            ),
            Text(state),
            Container(
              height: 40.0,
              width: double.infinity,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(6.0)),
                  color: Colors.black),
              child: GestureDetector(
                onTap: () {
                  makePayment();
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      CustomIcon.Custom.apple_pay,
                      color: Colors.white,
                      size: 40.0,
                    ),
                    SizedBox(
                      width: 8.0,
                    ),
                  ],
                ),
              ),
            ),
            Icon(
              Icons.favorite,
              color: color,
              size: 24.0,
            ),
          ]),
        ),
      ),
    );
  }
}
